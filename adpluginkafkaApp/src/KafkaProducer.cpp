/** Copyright (C) 2017 European Spallation Source */

/** @file  KafkaProducer.cpp
 *  @brief Implementation of a Kafka producer used with an areaDetector plugin.
 */

#include "KafkaProducer.h"
#include <algorithm>
#include <cassert>
#include <ciso646>
#include <cstdlib>
#include <fstream>
#include "spdlog/spdlog.h"
#include "epicsExit.h"
#include <boost/algorithm/string.hpp>

namespace KafkaInterface {

KafkaProducer::KafkaProducer(std::string topic,
                             ParameterHandler *ParamHandler,
                             const std::string &configFilePath) :
      conf(RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL)),
      tconf(RdKafka::Conf::create(RdKafka::Conf::CONF_TOPIC)),
      TopicName(std::move(topic)),
      ParamHandler(ParamHandler) {
  ParamHandler->registerParameter(&ReconnectFlush);
  ParamHandler->registerParameter(&ReconnectFlushTime);
  ParamHandler->registerParameter(&MsgBufferSize);
  ParamHandler->registerParameter(&MaxMessageSize);
  ParamHandler->registerParameter(&UnsentPackets);
  ParamHandler->registerParameter(&KafkaStatus);
  ParamHandler->registerParameter(&KafkaMessage);
  ParamHandler->registerParameter(&KafkaTopic);
  ParamHandler->registerParameter(&KafkaBroker);
  ParamHandler->registerParameter(&KafkaStatsInterval);
  ParamHandler->registerParameter(&KafkaQueueSize);
  InitRdKafka(configFilePath);
  MakeConnection();
}

KafkaProducer::~KafkaProducer() {
  if (runThread) {
    runThread = false;
    statusThread.join();
  }
}

bool KafkaProducer::StartThread() {
  if (not runThread) {
    if (errorState) {
      SetConStat(KafkaProducer::ConStat::ERROR,
                 "Unable to init kafka sub-system.");
      return false;
    }
    SetConStat(KafkaProducer::ConStat::DISCONNECTED, "Starting status thread.");
    runThread = true;
    statusThread = std::thread(&KafkaProducer::ThreadFunction, this);
    return true;
  }
  return false;
}

void KafkaProducer::ThreadFunction() {
  // Uses std::this_thread::sleep_for() as it can not know if a producer has
  // been allocated.
  while (runThread) {
    std::this_thread::sleep_for(PollSleepTime);
    {
      std::lock_guard<std::mutex> lock(brokerMutex);
      if (Producer != nullptr) {
        Producer->poll(0);
      }
    }
  }
}

bool KafkaProducer::SetMaxMessageSize(size_t msgSize) {
  if (errorState or 0 == msgSize) {
    return false;
  }
  RdKafka::Conf::ConfResult configResult1, configResult2;
  configResult1 =
      conf->set("message.max.bytes", std::to_string(msgSize), errstr);
  configResult2 =
      conf->set("message.copy.max.bytes", std::to_string(msgSize), errstr);
  if (RdKafka::Conf::CONF_OK != configResult1 or
      RdKafka::Conf::CONF_OK != configResult2) {
    SetConStat(KafkaProducer::ConStat::ERROR,
               "Unable to set max message size.");
    return false;
  }
  maxMessageSize = msgSize;
  MakeConnection();
  return true;
}

bool KafkaProducer::SetMessageBufferSizeKbytes(size_t msgBufferSize) {
  if (errorState or 0 == maxMessageBufferSizeKb) {
    return false;
  }
  if(!SetBrokerParam("queue.buffering.max.kbytes", std::to_string(maxMessageBufferSizeKb)))
    epicsExit(0);
  MakeConnection();
  return true;
}

size_t KafkaProducer::GetMessageBufferSizeKbytes() {
  std::stringstream buffer_size(GetBrokerParam("queue.buffering.max.kbytes"));
  size_t result;
  buffer_size >> result;
  return result;
}

size_t KafkaProducer::GetMaxMessageSize() { return maxMessageSize; }

bool KafkaProducer::SetMessageQueueLength(int queue) {
  if (errorState or 0 >= queue) {
    return false;
  }
  if(!SetBrokerParam("queue.buffering.max.messages", std::to_string(queue)))
    epicsExit(0);
  MakeConnection();
  return true;
}

int KafkaProducer::GetMessageQueueLength() {
  return std::stoi(GetBrokerParam("queue.buffering.max.messages"));
}

bool KafkaProducer::SendKafkaPacket(const unsigned char *buffer,
                                    size_t buffer_size, time_point Timestamp) {
  if (errorState or 0 == buffer_size) {
    return false;
  }
  if (buffer_size > maxMessageSize) {
    bool success = SetMaxMessageSize(buffer_size);
    if (not success) {
      errorState = true;
      return false;
    }
  }
  std::lock_guard<std::mutex> lock(brokerMutex);
  if (nullptr == Producer) {
    return false;
  }
  auto MessageTime = std::chrono::duration_cast<std::chrono::milliseconds>(
                         Timestamp.time_since_epoch())
                         .count();
  RdKafka::ErrorCode resp = Producer->produce(
      TopicName, -1, RdKafka::Producer::RK_MSG_COPY /* Copy payload */,
      const_cast<unsigned char *>(buffer), buffer_size, nullptr, 0, MessageTime,
      nullptr);

  if (RdKafka::ERR_NO_ERROR != resp) {
    SetConStat(KafkaProducer::ConStat::ERROR,
               "Producer failed with error code: " + std::to_string(resp));
    return false;
  }
  return true;
}

void KafkaProducer::event_cb(RdKafka::Event &event) {
  /// @todo This member function really needs some expanded capability
  switch (event.type()) {
  case RdKafka::Event::EVENT_ERROR:
    if (event.err() == RdKafka::ERR__ALL_BROKERS_DOWN) {
      SetConStat(KafkaProducer::ConStat::DISCONNECTED,
                 "Brokers down. Attempting to reconnect.");
    } else {
      SetConStat(KafkaProducer::ConStat::DISCONNECTED,
                 "Event error received: " + std::to_string(event.err()));
    }
    break;
  case RdKafka::Event::EVENT_LOG:
    /// @todo Add message/log or something
    break;
  case RdKafka::Event::EVENT_THROTTLE:
    /// @todo Add message/log or something
    break;
  case RdKafka::Event::EVENT_STATS:
    ParseStatusString(event.str());
    break;
  default:
    if ((event.type() == RdKafka::Event::EVENT_LOG) and
        (event.severity() == RdKafka::Event::EVENT_SEVERITY_ERROR)) {
      /// @todo Add message/log or something

    } else {
      /// @todo Add message/log or something
    }
  }
}

void KafkaProducer::SetConStat(KafkaProducer::ConStat stat,
                               std::string const &Msg) {
  CurrentStatus = stat;
  KafkaStatus.updateDbValue();
  ConnectionMessage = Msg;
  KafkaMessage.updateDbValue();
}

void KafkaProducer::ParseStatusString(std::string const &msg) {
  /// @todo We should probably extract some more stats from the JSON message
  const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
  JSONCPP_STRING err;
  bool parseSuccess =
      reader->parse(msg.c_str(), msg.c_str() + msg.size(), &root, &err);
  if (not parseSuccess) {
    SetConStat(KafkaProducer::ConStat::ERROR, "Status msg.: Unable to parse.");
    return;
  }
  brokers = root["brokers"]; // Contains broker information, including
                             // connection state
  if (brokers.isNull() or brokers.empty()) {
    SetConStat(KafkaProducer::ConStat::ERROR, "Status msg.: No brokers.");
  } else {
    KafkaProducer::ConStat tempStat = KafkaProducer::ConStat::DISCONNECTED;
    std::string statString = "Brokers down. Attempting reconnection.";
    if (std::any_of(brokers.begin(), brokers.end(),
                    [](Json::Value const &CBrkr) {
                      return "UP" == CBrkr["state"].asString();
                    })) {
      tempStat = KafkaProducer::ConStat::CONNECTED;
      statString = "No errors.";
    }
    SetConStat(tempStat, statString);
  }
  UnsentMessages = root["msg_cnt"].asInt();
  UnsentPackets.updateDbValue();
}

void KafkaProducer::AttemptFlushAtReconnect(bool flush) { doFlush = flush; }

void KafkaProducer::FlushTimeout(int32_t TimeOutMS) {
  flushTimeout = TimeOutMS;
}

bool KafkaProducer::SetBrokerParam(const std::string key, const std::string value) {
  spdlog::info("Setting \e[1m{}\e[0m broker parameter.", key);

  if (RdKafka::Conf::CONF_OK != conf->set(key, value, errstr)) {
    errorState = true;
    SetConStat(KafkaProducer::ConStat::ERROR, "Can not set broker parameter.");
    spdlog::error("Failed to set \e[1m{}\e[0m", key);
    return false;
  }
  return true;
}

std::string KafkaProducer::GetBrokerParam(const std::string key) {
  std::string value;

  if (RdKafka::Conf::CONF_OK != conf->get(key, value)) {
    errorState = true;
    SetConStat(KafkaProducer::ConStat::ERROR, "Can not get broker parameter.");
    spdlog::error("Failed to get \e[1m{}\e[0m", key);
    return "";
  }
  return value;
}

bool KafkaProducer::ConfigFromFile(const std::string &fname) {
 std::ifstream infile(fname);

  if (!infile.is_open()) {
    spdlog::error("Failed to load file \e[1m{}\e[0m.", fname);
    return false;
  }
  else {
    spdlog::info("File \e[1m{}\e[0m loaded successfully.", fname);
  }

  std::string line;
  
  while(std::getline(infile, line)) {

    std::istringstream is_line(line);
    std::string key;

    if(std::getline(is_line, key, '=')) {

      // Remove spaces on the left and right
      boost::algorithm::trim(key);

      // Ignore if it's a comment
      if(key.at(0) == '#')
        continue;

      std::string value;

      if(std::getline(is_line, value)) {

        // Remove spaces on the left and right
        boost::algorithm::trim(value);

        if(!SetBrokerParam(key, value))
          return false;
      } 
    }
  }
  return true;
}

void KafkaProducer::InitRdKafka(const std::string configFilePath) {
  if (nullptr == conf) {
    errorState = true;
    SetConStat(KafkaProducer::ConStat::ERROR,
               "Can not create global conf object.");
    return;
  }

  if (nullptr == tconf) {
    errorState = true;
    SetConStat(KafkaProducer::ConStat::ERROR,
               "Can not create topic conf object.");
    return;
  }

  if(!ConfigFromFile(configFilePath)) {
    epicsExit(0);
  }

  RdKafka::Conf::ConfResult configResult;
  configResult = conf->set("event_cb", this, errstr);
  if (RdKafka::Conf::CONF_OK != configResult) {
    errorState = true;
    SetConStat(KafkaProducer::ConStat::ERROR, "Can not set event callback.");
    return;
  }

  if(!SetBrokerParam("message.max.bytes", std::to_string(maxMessageSize)))
    epicsExit(0);

  if(!SetBrokerParam("message.copy.max.bytes", std::to_string(maxMessageSize)))
    epicsExit(0);
}

bool KafkaProducer::SetStatsTimeMS(int time) {
  // We do not set the appropriate PV here as this is done in KafkaDriver.
  if (errorState or time <= 0) {
    return false;
  }
  if(!SetBrokerParam("statistics.interval.ms", std::to_string(time)))
    epicsExit(0);
  MakeConnection();
  return true;
}

int KafkaProducer::GetStatsTimeMS() {
  return std::stoi(GetBrokerParam("statistics.interval.ms")); 
}

bool KafkaProducer::SetTopic(std::string const &NewTopicName) {
  if (NewTopicName.empty()) {
    return false;
  }
  TopicName = NewTopicName;
  return true;
}

std::string KafkaProducer::GetTopic() { return TopicName; }

bool KafkaProducer::SetBrokerAddr(std::string const &NewBrokerAddr) {
  if (errorState or NewBrokerAddr.empty()) {
    return false;
  }
  if(!SetBrokerParam("metadata.broker.list", NewBrokerAddr))
    epicsExit(0);
  MakeConnection();
  return true;
}

std::string KafkaProducer::GetBrokerAddr() { 
  return GetBrokerParam("metadata.broker.list"); 
}

bool KafkaProducer::MakeConnection() {
  // Do we know for sure that all possible paths will work? No!
  // This code could probably be improved somewhat.
  std::lock_guard<std::mutex> lock(brokerMutex);
  if (GetBrokerAddr().empty()) {
    spdlog::warn("Broker address is empty.");
  }
  else {
    Producer.reset(RdKafka::Producer::create(conf.get(), errstr));
    if (nullptr == Producer) {
      SetConStat(KafkaProducer::ConStat::ERROR, "Unable to create producer.");
      spdlog::error("Unable to create producer.");
      return false;
    }
  }
  SetConStat(KafkaProducer::ConStat::CONNECTING, "Trying to open Kafka connection.");
  return true;
}
} // namespace KafkaInterface
