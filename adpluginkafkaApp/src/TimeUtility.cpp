/** Copyright (C) 2020 European Spallation Source */

/** @file  TimeUtility.cpp
 *  @brief Some time utility code.
 */

#include "TimeUtility.h"

std::uint64_t timeStampToNsec(epicsTimeStamp const &Timestamp) {
  const auto NSecMultiplier = 1000000000L;
  return static_cast<std::uint64_t>(Timestamp.secPastEpoch) *
             NSecMultiplier +
         Timestamp.nsec;
}

time_point timeStampToTimePoint(epicsTimeStamp const &TimeStamp) {
  time_point ReturnValue{std::chrono::nanoseconds(timeStampToNsec(TimeStamp))};
  return ReturnValue;
}