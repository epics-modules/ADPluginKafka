# ---
# Test startup script
require adpluginkafka
require adsimdetector
require busy
require ndpluginschemas

# Prefix for all records
epicsEnvSet("PREFIX", "ioc:")

# The port name for the detector
epicsEnvSet("PORT",   "SIM1")

# The maximum image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "128")

# The maximum image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "128")

asynSetMinTimerPeriod(0.001)

simDetectorConfig("$(PORT)", $(XSIZE), $(YSIZE), 1, 0, 0)

dbLoadRecords("$(adsimdetector_DIR)/db/simDetector.template", "P=$(PREFIX), R=lab:, PORT=$(PORT), ADDR=0, TIMEOUT=1")

# NDStdArray plugin
NDStdArraysConfigure("Image1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)

# This creates a waveform large enough for 2000x2000x3 (e.g. RGB color) arrays.
# This waveform only allows transporting 8-bit images
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int8,FTVL=UCHAR,NELEMENTS=12000000")

# Schemas plugin

NDSchemasConfigure("NDSCHEMA", 10, 1, "$(PORT)", 0, -1, 10000, "some_source")
dbLoadRecords("NDPluginSchemas.template", "P=$(PREFIX), R=NDSchema:, PORT=NDSCHEMA, ADDR=0, TIMEOUT=1, NDARRAY_PORT=$(PORT)")

asynSetTraceMask NDSCHEMA -1 "FLOW+ERROR"

# Kafka plugin

epicsEnvSet("KFK_TOPIC", "ymir_camera")
epicsEnvSet("KFK_CONFIG_FILE_PATH", "/home/georgekontogiorgos/git/ADPluginKafka/kafka.conf")

KafkaPluginConfigure("KFK1", 3, 1, "NDSCHEMA", 0, -1, "$(KFK_TOPIC)","$(KFK_CONFIG_FILE_PATH)")
dbLoadRecords("$(adpluginkafka_DIR)db/adpluginkafka.db", "P=$(PREFIX), R=kfk1:, PORT=KFK1, ADDR=0, TIMEOUT=1, NDARRAY_PORT=NDSCHEMA")

asynSetTraceMask KFK1 -1 "FLOW+ERROR"

iocInit

dbpf ioc:NDSchema:EnableCallbacks 1
dbpf ioc:kfk1:EnableCallbacks 1
dbpf ioc:lab:AcquirePeriod 1
# ...
