
adpluginkafka
======
European Spallation Source ERIC Site-specific EPICS module : adpluginkafka


# Pre configuration

The kafka plugin will require a `kafka.conf` alike file to set the connection to the Kafka Brokers like:

```bash
security.protocol=sasl_ssl
sasl.mechanism=SCRAM-SHA-256
sasl.password=**************
sasl.username=**************
ssl.ca.location=/etc/ssl/certs/ecdc-kafka-ca.crt
ssl.endpoint.identification.algorithm=none
bootstrap.servers=14.130.2.15:8092,15.110.4.15:8092
statistics.interval.ms=500
queue.buffering.max.kbytes=2500000
```

That file can be installed and managed automatically by the ansible playbook [ics-ans-adpluginkafka](https://gitlab.esss.lu.se/nice/nice-ansible-galaxy/ics-ans-adpluginkafka) that can be triggered by this [AWX template](https://torn.tn.esss.lu.se/#/templates/job_template/1080/details). Remember to configure your host on the inventory, its variables and the groups the host belongs to. You might need to create new groups for different brokers settings. The playbook also installs the SSL certificates if needed, it will be fetch from [Hashicorp Vault](https://hashicorp-vault.tn.esss.lu.se:8200/).

Note that there is a character limitation on the EPICS side for the Kafka parameters. You might found issues if setting big names. If long paramenters are needed and the IOC is failing due to that, ask the integrators to change the PVs from string type to waveform type. Bootstrap servers are already modified to support 255 characters, which translates into 11 hosts separeted by comma only. 
